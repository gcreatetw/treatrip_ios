//
//  ScrollView+ContentView.swift
//  scrollView_snp_layout
//
//  Created by TANG,QI-RONG on 2021/5/12.
//

import UIKit

public enum ScrollViewType {
    case vertical, horizontal
}

open class BasicScrollView: UIScrollView {
    public let scrollContentView = UIView()
    public var type: ScrollViewType = .vertical
        
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
        
    public init(_ type: ScrollViewType) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.translatesAutoresizingMaskIntoConstraints = false
        self.type = type
        setupViews()
    }
        
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupViews() {
        self.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            self.contentInsetAdjustmentBehavior = .never
        }else {
            //Fallback on earlier versions
        }
        
        self.addSubview(scrollContentView)
        
        let layouts = [scrollContentView.leadingAnchor.constraint(equalTo: leadingAnchor),
                       scrollContentView.trailingAnchor.constraint(equalTo: trailingAnchor),
                       scrollContentView.topAnchor.constraint(equalTo: topAnchor),
                       scrollContentView.bottomAnchor.constraint(equalTo: bottomAnchor)]
        
        NSLayoutConstraint.activate(layouts)
        
        switch type {
        case .vertical:
            scrollContentView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
            let scrollViewHeightConstraint = scrollContentView.heightAnchor.constraint(equalTo: self.heightAnchor)
            scrollViewHeightConstraint.priority = UILayoutPriority(250)
            scrollViewHeightConstraint.isActive = true
            
        case .horizontal:
            scrollContentView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
            let scrollViewWidthConstraint = scrollContentView.widthAnchor.constraint(equalTo: self.heightAnchor)
            scrollViewWidthConstraint.priority = UILayoutPriority(250)
            scrollViewWidthConstraint.isActive = true
        }
    }
}

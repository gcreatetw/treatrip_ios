//
//  PhotoCollection.swift
//  banner_practice
//
//  Created by TANG,QI-RONG on 2021/3/26.
//

import Foundation

class Photo {
    
    static let share: Photo = {
        let instance = Photo()
        
        return instance
    }()
    
    private init() {}
    
    var sharePhoto = ["home_bn03", "home_bn01", "home_bn02", "home_bn03", "home_bn01"]
    
}

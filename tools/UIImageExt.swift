//
//  UIImageExt.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/12.
//

import Foundation
import UIKit

extension UIImageView {
    @IBInspectable var svgTintColor: UIColor? {
        get { return nil }
        set(key) {
            if let image = image?.withRenderingMode(.alwaysTemplate) {
                self.image = image
                tintColor = key
            }
        }
    }
}

extension UIImage {
    convenience init?(svg_name: String, tintColor: UIColor? = nil) {
        self.init(named: svg_name)
        if let tintColor = tintColor {
            self.withTintColor(tintColor)
        }
    }
}

extension String {
    public func toSVGUIImage(tintColor: UIColor?)-> UIImage?{
        return UIImage(svg_name: self, tintColor: tintColor)
    }
}

//
//  WebSiteViewController.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/28.
//

import UIKit

import UIKit
import WebKit

class WebSiteViewController: BaseViewController , WKUIDelegate{

    @IBOutlet weak var vMain: UIView!
    
    let webConfiguration = WKWebViewConfiguration()
    lazy var webView: WKWebView = {
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: 0.0, height: vMain.frame.size.height))
//        WKWebView(frame: .zero, configuration: webConfiguration)
        return WKWebView(frame: customFrame, configuration: webConfiguration)
    }()
    
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initEvent()
        initWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUrl(url: url)
    }
    
    private func initEvent(){
        
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    private func initWebView(){
        webView.translatesAutoresizingMaskIntoConstraints = false
        //關掉webView的view超出介面滑動跟縮放
        webView.scrollView.bounces = false
        webView.scrollView.bouncesZoom = false
        webView.scrollView.delegate = self
        vMain.addSubview(webView)
        webView.topAnchor.constraint(equalTo: vMain.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: vMain.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: vMain.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: vMain.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: vMain.heightAnchor).isActive = true
        webView.uiDelegate = self
    }
    
    func loadUrl(url: String){
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension WebSiteViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}


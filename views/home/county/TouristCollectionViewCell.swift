//
//  TownsCollectionViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/6/1.
//

import UIKit

class TouristCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var touristImageView: UIImageView!
    @IBOutlet weak var touristLabel: UILabel!
        
    override func awakeFromNib() {
        initView()
    }
    
    func initView(){
        
    }
    
    func setValue(_ data: CountyPageModel.RecommendAttractions) {
        touristLabel.text = data.post_title ?? ""
        touristImageView.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
    }
}

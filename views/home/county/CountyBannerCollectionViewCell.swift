//
//  CountyBannerCollectionViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/31.
//

import UIKit

class CountyBannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerLabel: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: CountyPageModel.Towns) {
        bannerLabel.text = data.township ?? ""
    }
}


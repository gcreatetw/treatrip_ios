//
//  CountyViewController.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/31.
//

import UIKit

class CountyPageViewController: UIViewController {

    private var locationList = LocationData.share.data
    private var countyList = [CountyPageModel.Towns]()
    //let data = HomeLocationData.self
    var region = String()
    var id = Int()
    
    @IBOutlet weak var countyTableView: UITableView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var mainImageView: UIImageView!
    
    @IBAction func toGoButtonAct(_ sender: Any) {
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        
        countyTableView.delegate = self
        countyTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        syncApi()
    }
    
    func setValue(_ data: HomeLocationData){
//        id = data.id
//        region = data.name
    }
    
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.countyPage(region: self.region, id: self.id)
            if response.isSuccess {
                self.countyList = response.data?.towns ?? []
                DispatchQueue.main.async {
                    self.mainImageView.kf.setImage(with: URL(string: response.data?.region_banner_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
                    self.bannerCollectionView.reloadData()
                    self.countyTableView.reloadData()
                }
            }
        }
    }
    
}


extension CountyPageViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bannerCollectionView {
            return countyList.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: CountyBannerCollectionViewCell.self, for: indexPath)
        
        if indexPath.row < countyList.count {
            cell.setValue(countyList[indexPath.row])
        }
        return cell
    }
}

extension CountyPageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(CountyTableViewCell.self)", for: indexPath) as! CountyTableViewCell
        
        if indexPath.row < countyList.count {
            cell.setValues(countyList[indexPath.row])
        }
        
        return cell
    }
    
    
}

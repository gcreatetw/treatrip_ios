//
//  CountyTableViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/6/1.
//

import UIKit

class CountyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var townsLabel: UILabel!
    @IBOutlet weak var townsCollectionView: UICollectionView!
    
    private var townsList = [CountyPageModel.RecommendAttractions]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
        
        townsCollectionView.delegate = self
        townsCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initView() {

    }
    
    func setValues(_ data: CountyPageModel.Towns ) {
        townsLabel.text = data.township ?? ""
        townsList = data.recommendAttractions ?? []
        townsCollectionView.reloadData()
    }

}

extension CountyTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return townsList.count 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: TouristCollectionViewCell.self, for: indexPath)
        
        if indexPath.row < townsList.count {
            cell.setValue((townsList[indexPath.row]))
        }
        
        return cell
    }
    
    
}

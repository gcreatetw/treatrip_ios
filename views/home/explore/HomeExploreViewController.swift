//
//  ExploreViewController.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/18.
//

import UIKit

class HomeExploreViewController: BaseViewController {

    @IBOutlet weak var SVmain: UIView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var exploreTableView: UITableView!
    
    @IBAction func toGoButtonAct(_ sender: Any) {
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    let iconImage = ["icon_taiwan_hc-1", "icon_taiwan_nw-1", "icon_taiwan_rec-1", "icon_taiwan_ee-1"]
     
    private var discoverTaiwanList = [ExploreModel.DiscoverTaiwan]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exploreTableView.delegate = self
        exploreTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
        syncApi()
        //exploreTableView.rowHeight = 500
        //exploreTableView.estimatedRowHeight = 0
    }
    
    private func initView(){
        bannerCollectionView?.apply {
            $0.dataSource = self
            $0.delegate = self
            $0.isPagingEnabled = true
        }
        
    }
    
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.explorePage()
            if response.isSuccess {
                self.discoverTaiwanList = response.data?.discover_taiwain ?? []
                DispatchQueue.main.async {
                    // reload data
                    self.bannerCollectionView.reloadData()
                    self.exploreTableView.reloadData()
                }
            }
        }
    }

}


extension HomeExploreViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bannerCollectionView {
            return discoverTaiwanList.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if collectionView == bannerCollectionView {
//
//        }
        let cell = collectionView.dequeueReusableCell(withClass: ExploreBannerCollectionViewCell.self, for: indexPath)
        if indexPath.row < discoverTaiwanList.count {
            cell.setValue(discoverTaiwanList[indexPath.row])
        }
        return cell
    }
    
}

extension HomeExploreViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoverTaiwanList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(ExploreTitleTableViewCell.self)", for: indexPath) as! ExploreTitleTableViewCell
        
        if indexPath.row < discoverTaiwanList.count {
            cell.setValue(discoverTaiwanList[indexPath.row])
        }
        
        cell.themeImageView.image = UIImage(named: iconImage[indexPath.row])
        
        return cell
    }
    
    
}

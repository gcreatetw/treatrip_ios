//
//  categoriesTableViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/27.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    private var catPostsList: [ExploreModel.CatPosts]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValue(_ data: ExploreModel.Catgories) {
        categoriesLabel.text = data.cat_title ?? ""
        catPostsList = data.cat_posts ?? []
        categoriesCollectionView.reloadData()
    }

}

extension CategoriesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        catPostsList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: CatPostsCollectionViewCell.self, for: indexPath)
        
        if indexPath.row < catPostsList?.count ?? 0{
            cell.setValue((catPostsList?[indexPath.row])!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "TaiwanDetail", bundle: nil)
        let catPostsData = catPostsList?[indexPath.row]
        if let next = storyboard.instantiateViewController(withClass: TaiwanDetailViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.postId = String(catPostsData?.ID ?? 0)
            next.postImageView = catPostsData?.post_img
            //self.present(next, animated: true)
        }
    }
    
}

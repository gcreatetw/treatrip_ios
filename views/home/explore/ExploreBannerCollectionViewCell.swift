//
//  BannerCollectionViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/20.
//

import UIKit

class ExploreBannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerLabel: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: ExploreModel.DiscoverTaiwan) {
        bannerLabel.text = data.main_type ?? ""
        
    }
}

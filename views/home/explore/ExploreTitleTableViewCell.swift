//
//  ExploreTitleTableViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/26.
//

import UIKit

class ExploreTitleTableViewCell: UITableViewCell {

    
    @IBOutlet weak var themeImageView: UIImageView!
    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var categoriesTableView: UITableView!
    
    private var categoriesList = [ExploreModel.Catgories]()
    
    //var someImageView: UIImageView?
   // var svgImage: SVGKImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
        categoriesTableView.delegate = self
        categoriesTableView.dataSource = self
        //categoriesTableView.rowHeight = 200
        //categoriesTableView.estimatedRowHeight = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initView(){
        
    }
    
    func setValue(_ data: ExploreModel.DiscoverTaiwan) {
        
        themeLabel.text = data.main_type ?? ""
        categoriesList = data.catgories ?? []
        categoriesTableView.reloadData()
    }

}

extension ExploreTitleTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        categoriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(CategoriesTableViewCell.self)", for: indexPath) as! CategoriesTableViewCell
        
        if indexPath.row < categoriesList.count {
            cell.setValue(categoriesList[indexPath.row])
        }
        
        return cell
    }
    
    
}

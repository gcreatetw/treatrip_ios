//
//  CatPostsCollectionViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/27.
//

import UIKit

class CatPostsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var postsImageView: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
    
    func initView(){
        
    }
    
    func setValue(_ data: ExploreModel.CatPosts) {
        postLabel.text = data.post_title ?? ""
        postsImageView.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
    }
}

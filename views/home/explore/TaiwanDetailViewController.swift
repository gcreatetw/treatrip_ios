//
//  TaiwanDetailViewController.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/25.
//

import UIKit

class TaiwanDetailViewController: UIViewController {

    @IBOutlet weak var taiwanDetailImageView: UIImageView!
    
    var postId: String?
    var postImageView: String?
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        syncApi()
        taiwanDetailImageView.image = UIImage(named: postImageView ?? "")
    }

    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.taiwanDetailPage(post_id: self.postId ?? "尚無資料")
            if response.isSuccess {
               // self.discoverTaiwanList = response.data?.discover_taiwain ?? []
                DispatchQueue.main.async {
                    
                   // self.bannerCollectionView.reloadData()
                }
            }
        }
    }
    

}

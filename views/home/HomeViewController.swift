//
//  HomeViewController.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/15.
//

import UIKit
import SnapKit

class HomeViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var svMain: UIScrollView!
    @IBOutlet weak var svMainView: UIView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchBarBackgroundView: UIView!
    @IBOutlet weak var collectionLocation: UICollectionView!
    @IBOutlet weak var collectionLimitedTime: UICollectionView!
    @IBOutlet weak var collectionPopularAttractions: UICollectionView!
    
    
    @IBAction func toGoButtonAct(_ sender: Any) {
    }
    
    @IBAction func adventureThemeButtonClick(_ sender: Any) {
        let controller = UIStoryboard(name: "AdventureTheme", bundle: nil)
        let vc = controller.instantiateViewController(identifier: "HomeAdventureThemeViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: .none)
    }
    
    @IBAction func exploreButtonClick(_ sender: Any) {
        let controller = UIStoryboard(name: "Explore", bundle: nil)
        let vc = controller.instantiateViewController(identifier: "HomeExploreViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: .none)
    }
    
    
    let basicScrollView = BasicScrollView(.horizontal)
    var timer = Timer()
    let width = UIScreen.main.bounds.size.width
    var lastContentOffset: CGFloat = 0.0
    var currentIndex : Int = 1
    var imageArray: [String]! {
        didSet {
            self.pageControl.currentPage = imageArray.count
            //self.setupImageView()
        }
    }
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.frame = CGRect(x: 0, y: basicScrollView.frame.maxY - 20, width: width, height: 30)
        pageControl.backgroundColor = UIColor.clear
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pageControl.numberOfPages = imageArray.count - 2
        //取消按住拖曳的功能
        pageControl.isUserInteractionEnabled = false
        
        return pageControl
    }()
    
    func setupBanner() {
       
        //page手動滑動部分待決
        self.view.addSubview(pageControl)
        self.view.sendSubviewToBack(pageControl)
        self.view.addSubview(basicScrollView)
        self.view.sendSubviewToBack(basicScrollView)
        
        basicScrollView.showsVerticalScrollIndicator = true
        basicScrollView.showsHorizontalScrollIndicator = true
        basicScrollView.bounces = true
        basicScrollView.isPagingEnabled = true
        basicScrollView.isScrollEnabled = true
        basicScrollView.contentSize = CGSize(width: imageArray.count + 1, height: 2)
        //scrollView.frame = CGRect(x: 0, y: 50, width: width, height: self.view.frame.height)
                
        //可在添加CGFloat(imageArray.count + 添加頁面
        basicScrollView.contentSize = CGSize(width: width * CGFloat(imageArray.count + 0), height: 0)
        basicScrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
        basicScrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: 0), animated: true)
        
        //Layout
        basicScrollView.snp.makeConstraints { (make) in
            make.right.left.equalTo(self.view)
            make.top.equalTo(self.view).offset(40)
        }
    }
    
    func setupImageView() {
            //匯入輪播圖片
            let cc = [UIColor.red, UIColor.gray, UIColor.yellow, UIColor.green, UIColor.brown]
            for index in 0...(imageArray.count - 1) {
                let w =  self.view.frame.width
                let imageView = UIImageView(frame: CGRect(x: self.view.frame.width * CGFloat(index), y: 0, width: self.view.frame.width, height: 260 ))
               
                var picName = String()
                print("setupScroll Index = \(index)")
                switch index {
                case 0:
                    //首張放在最後
                    picName = imageArray.last!
                    print("case 0")
                    break
                case imageArray.count - 1:
                    //最後一張
                    picName = imageArray.first!
                    print("case 最後一張為第一張")
                    break
                default:
                    //中間
                    picName = imageArray[index - 1]
                    print("switch default")
                    break
                }
                imageView.backgroundColor = cc[index]
                imageView.image = UIImage(named: imageArray[index])
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                imageView.layer.masksToBounds = true
                imageView.isUserInteractionEnabled = true
                
                basicScrollView.scrollContentView.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width) * imageArray.count + 1, height: 260)
                basicScrollView.scrollContentView.addSubview(imageView)
                basicScrollView.translatesAutoresizingMaskIntoConstraints = false
                basicScrollView.scrollContentView.backgroundColor = #colorLiteral(red: 0.348926554, green: 0.4032353505, blue: 0.6181768408, alpha: 1)
                basicScrollView.backgroundColor = #colorLiteral(red: 0.4463793877, green: 1, blue: 0.809113567, alpha: 1)
                
//                imageView.snp.makeConstraints { (make) in
//                   // make.height.equalTo(scrollView.scrollContentView.frame.height)
//                    make.edges.equalTo(basicScrollView.scrollContentView).inset(UIEdgeInsets(top: 0, left: -50, bottom: 0, right: -50))
//                    //make.top.equalTo(basicScrollView.scrollContentView)
//                }
            }
            
        }
    
    //自動＆手動滑動皆會偵測到
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.basicScrollView {
            //隨著圖片往右移動 寬度增加
            let contentOffsetX = scrollView.contentOffset.x
            //print(contentOffsetX)
            //永遠固定寬度
            //print(scrollView.frame.width)
    
            //移至最後一張
            if contentOffsetX == CGFloat(self.imageArray.count - 5) * self.view.frame.width {
                scrollView.contentOffset = CGPoint(x: self.view.frame.width * CGFloat(3), y: 0)
                currentIndex = 3
                print("最後一張圖")
            }
            // 跑到最後一張圖，回到第0張圖
            if contentOffsetX == CGFloat(self.imageArray.count - 1) * self.view.frame.width {
                scrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
                print("回到第0張圖")
            }
            let index = round(scrollView.contentOffset.x / width) - 1
            self.pageControl.currentPage = Int(index)
            
        }
        if scrollView == self.svMain{
            //searchBarView顯示顏色
            let imageOriginalHeight: CGFloat = 260
            let moveDistance = abs(scrollView.contentOffset.y)
            //print("svMain.contentOffset.y = \(scrollView.contentOffset.y)")
            //print("moveDistance = \(moveDistance)")
            if scrollView.contentOffset.y > 0 {
                searchBarBackgroundView.backgroundColor = UIColor(red: 237/255, green: 116/255, blue: 101/255, alpha: moveDistance / imageOriginalHeight)
            }
            
            if scrollView.contentOffset.y > CGFloat(195) {
                self.searchBarView.layer.masksToBounds = true
                self.svMainView.layer.cornerRadius = 0
                //print("陰影關閉")
            }else if scrollView.contentOffset.y < CGFloat(195) {
                self.searchBarView.layer.masksToBounds = false
                self.svMainView.layer.cornerRadius = 15
                //print("陰影開啟")
            }
            //圓角
            if scrollView.contentOffset.y > CGFloat(260) {
                self.svMainView.layer.cornerRadius = 0
            }else if scrollView.contentOffset.y < CGFloat(260) {
                self.svMainView.layer.cornerRadius = 15
            }
        }
        
    }
    
    func setPageControl() {
        //self.view.addSubview(pageControl)
        //basicScrollView.sendSubviewToBack(pageControl)
       // self.view.sendSubviewToBack(pageControl)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(240)
            make.right.left.equalToSuperview().offset(0)
        }
    }
    
    func setSearchBarShadow() {
        self.searchBarView.layer.masksToBounds = false
        self.searchBarBackgroundView.layer.shouldRasterize = true
        self.searchBarView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.searchBarView.layer.shadowOffset = CGSize(width: 0,height: 2)
        self.searchBarView.layer.shadowOpacity = 0.5
        self.searchBarView.layer.shadowRadius = 15
        self.searchBarView.layer.shadowPath = UIBezierPath(rect: searchBarView.bounds).cgPath
    }
    
    //MARK: - Timer
               
        func setTimer() {
            timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
        }
        
        @objc func autoScroll() {
            
            if currentIndex == imageArray.count - 1 {
                currentIndex = 2
                print("autoScroll 第一頁")
            }else {
                currentIndex += 1
                print("Index += 1")
            }
                            
            basicScrollView.setContentOffset(CGPoint(x: self.view.frame.width * CGFloat(currentIndex), y: 0), animated: true)
            print("自動換頁")
             //reloadImage()
         }
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            if scrollView == self.basicScrollView {
                print("手滑移動！")
                setTimer()
            }
            
        }

        func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            if scrollView == self.basicScrollView {
                //暫停計時
                lastContentOffset = lastContentOffset - 1
                timer.invalidate()
                print("停止計時")
            }
        }
        
        func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {

            let point = scrollView.panGestureRecognizer.velocity(in: scrollView)
            if scrollView == self.basicScrollView {
                if currentIndex == imageArray.count - 1 {
                    currentIndex = 2
                    print("Gesture 第一頁")
                    
                }else if point.x > 0 {
                    currentIndex = currentIndex - 1
                    print("向左")
                }else {
                    currentIndex = currentIndex + 1
                    print("向右")
                }
            }
        }
    
    //MARK: - 圖片
    private let locationList = LocationData.share.data
    private var limitedTimeList = [HomePageModel.LimitedTime]()
    private var popularAttractionsList = [HomePageModel.PopularAttractions]()
    
//MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()
        basicScrollView.delegate = self
        svMain.delegate = self
        imageArray = Photo.share.sharePhoto
       
        setSearchBarShadow()
        setupBanner()
        setPageControl()
        setupImageView()
        setTimer()
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initData()
        initView()
        setValue()
        syncApi()
        reloadScrollView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
 
    }
    
    private func initData(){
        
    }
    
    private func initView(){

        //
        collectionLocation?.apply {
            $0.delegate = self
            $0.dataSource = self
        }
        //
        collectionLimitedTime?.apply {
            $0.delegate = self
            $0.dataSource = self
        }
        //
        collectionPopularAttractions?.apply {
            $0.delegate = self
            $0.dataSource = self
        }
    }
    
    private func setValue(){
        
    }
    
    private func reloadScrollView(){

        if let bottomView = svMainView.subviews.max(by: { $1.y > $0.y }) {
//            log(TAG, "bottomView \(bottomView.y + bottomView.bounds.height)")
            let constraintHeight = svMainView.constraints.first { $0.firstAttribute == .height }
            constraintHeight?.constant = bottomView.y + bottomView.bounds.height + 20
            svMainView.layoutIfNeeded()
        }
    }
    //取出資料 做更新畫面
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.postHomePage()
            if response.isSuccess {
                self.limitedTimeList = response.data?.limited_time ?? []
                self.popularAttractionsList = response.data?.popular_attractions ?? []
                DispatchQueue.main.async {
                    // reload data
                    self.collectionLimitedTime?.reloadData()
                    self.collectionPopularAttractions?.reloadData()
                    self.reloadScrollView()
                }
            }
        }
    }

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionLocation {
            return locationList.count
        } else if collectionView == collectionLimitedTime {
            return limitedTimeList.count
        } else if collectionView == collectionPopularAttractions {
            return popularAttractionsList.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionLimitedTime {
            // 期間限定
            let cell = collectionView.dequeueReusableCell(withClass: HomeLimitedTimeCollectionViewCell.self, for: indexPath)
            if indexPath.row < limitedTimeList.count {
                cell.setValue(limitedTimeList[indexPath.row])
            }
            return cell
        } else if collectionView == collectionPopularAttractions {
            // 爆紅景點
            let cell = collectionView.dequeueReusableCell(withClass: HomePopularAttractionsCollectionViewCell.self, for: indexPath)
            if indexPath.row < popularAttractionsList.count {
                cell.setValue(popularAttractionsList[indexPath.row])
            }
            return cell
        } else {
            // location(縣市)
            let cell = collectionView.dequeueReusableCell(withClass: HomeLocationCollectionViewCell.self, for: indexPath)
            if indexPath.row < locationList.count {
                cell.setValue(locationList[indexPath.row])
            }
            return cell
        }
    }
    
    //目前只有『期間限定』＆『爆紅景點』擁有頁面功能
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionLimitedTime {
            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.url = "https://www.treatrip.com/limited/hsinchu-lightcoming/"
                self.present(next, animated: true)
            }
        } else if collectionView == collectionPopularAttractions {
            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.url = "https://www.treatrip.com/limited/hsinchu-lightcoming/"
                self.present(next, animated: true)
            }
        } else if collectionView == collectionLocation {
            let storyboard = UIStoryboard.init(name: "CountyPage", bundle: nil)
            let data = locationList[indexPath.row]
            
            if let next = storyboard.instantiateViewController(withClass: CountyPageViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.id = data.id
                next.region = data.name
                self.present(next, animated: true, completion: nil)
            }
        }
    }
}

//
//  HomeAdventureThemeViewController.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/13.
//

import UIKit

class HomeAdventureThemeViewController: BaseViewController {
    
    var fullScreenSize: CGSize!
    private var adventureList = [HomeAdventureThemeModel.Adventure]()
    
    @IBOutlet weak var themeCollectionView: UICollectionView!
    
    
    @IBAction func toGoButtonAct(_ sender: Any) {
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    func configureCellSize() {
        let itemSpace: CGFloat = 0
        fullScreenSize = UIScreen.main.bounds.size
        let flowLayout = themeCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        
        flowLayout?.itemSize = CGSize(width: fullScreenSize.width / 2, height: themeCollectionView.frame.height / 2)
        flowLayout?.estimatedItemSize = .zero
        flowLayout?.minimumInteritemSpacing = itemSpace
        flowLayout?.minimumLineSpacing = itemSpace
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCellSize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        syncApi()
        initView()
    }
    
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.postHomeAdventureTheme(page: 1)
            if response.isSuccess {
                self.adventureList = response.data?.adventure ?? []
                
//                self.travelRecommendList = response.data?.travel_recommend ?? []
                DispatchQueue.main.async {
                    self.themeCollectionView.reloadData()

                }
            }
        }
    }
    
    private func initView() {
        themeCollectionView.apply {
            $0.dataSource = self
            $0.delegate = self
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeAdventureThemeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return adventureList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: HomeAdventureThemeCollectionViewCell.self, for: indexPath) 
        
        if indexPath.row < adventureList.count {
            cell.setValue(adventureList[indexPath.row])
        }
        
        let adventureData = adventureList[indexPath.row]
        cell.themeLabel.text = adventureData.post_title
        cell.themeImageView.kf.setImage(with: URL(string: adventureData.post_img ?? ""))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
        print(indexPath.row)
        let adventureData = adventureList[indexPath.row]
        if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.url = adventureData.post_url ?? ""
            self.present(next, animated: true, completion: nil)
        }
    }
    
}

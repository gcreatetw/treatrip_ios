//
//  HomeAdventureThemeCollectionViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/14.
//

import UIKit

class HomeAdventureThemeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var themeImageView: UIImageView!
    @IBOutlet weak var themeLabel: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: HomeAdventureThemeModel.Adventure) {
        themeImageView.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        themeLabel.text = data.post_title ?? ""
        
    }
}

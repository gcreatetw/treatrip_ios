//
//  HomeLocationCollectionViewCell.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/16.
//

import Foundation
import UIKit

class HomeLocationData {
    let name: String
    let en_name: String
    let image: String
    let id: Int
    init(_ name: String, _ en_name: String, _ image: String, id: Int) {
        self.name = name
        self.en_name = en_name
        self.image = image
        self.id = id
    }
}

class HomeLocationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbEnName: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: HomeLocationData){
        iv.image = UIImage(named: data.image)
        lbName.text = data.name
        lbEnName.text = data.en_name
    }
}

//
//  HomeLimitedTimeCollectionViewCell.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/17.
//

import UIKit

class HomeLimitedTimeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var lb: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: HomePageModel.LimitedTime){
        iv.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        lb.text = data.post_title ?? ""
    }
}

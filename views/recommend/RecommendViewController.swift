//
//  Recommend ViewController.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/15.
//

import UIKit

class RecommendViewController: BaseViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var svMain: UIScrollView!
    @IBOutlet weak var svMainView: UIView!
    @IBOutlet weak var vMain: UIStackView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfOpenTime: UITextField!
    @IBOutlet weak var tfWebsite: UITextField!
    @IBOutlet weak var tfCost: UITextField!
    @IBOutlet weak var tfTraffic: UITextField!
    @IBOutlet weak var tfNote: UITextView!

    //uploadPhotos
    @IBOutlet weak var uploadPhoto1: UIImageView!
    @IBOutlet weak var uploadPhoto2: UIImageView!
    @IBOutlet weak var uploadPhoto3: UIImageView!
    @IBOutlet weak var uploadPhoto4: UIImageView!
    @IBOutlet weak var uploadPhoto5: UIImageView!
    
    
    @IBAction func toGoButtonAct(_ sender: Any) {
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func recommendButtonAct(_ sender: Any) {
        uploadString()
        syncApi()
        
        if tfName.text == "" || tfAddress.text == "" || photoStringAry == ["", "", "", "", ""] {
            let alertController = UIAlertController(title: "必填欄位尚未填寫", message: "請確認必填欄位是否輸入完畢 \n \(apiMessage)", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
            print(tfName.text ?? "沒有輸入")
            print(tfAddress.text ?? "沒有輸入")
            //print(photoStringAry)
            
        }else if successInfo == false {
            let alertController = UIAlertController(title: "資料上傳失敗", message: "上傳狀態: \(successInfo)", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
            
        }else {
            
            let alertController = UIAlertController(title: "上傳資料成功", message: "", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func uploadPhoroButtonAct1(_ sender: Any) {
        uploadPhotos()
        if uploadImageView[0] != nil {
            uploadImageView[0] = nil
            print("按鈕1")
        }
    }
    
    @IBAction func uploadPhoroButtonAct2(_ sender: Any) {
        uploadPhotos()
        if uploadImageView[1] != nil {
            uploadImageView[1] = nil
            //uploadImageView.remove(at: 1)
            print("按鈕2")
        }
    }
    
    @IBAction func uploadPhoroButtonAct3(_ sender: Any) {
        uploadPhotos()
        if uploadImageView[2] != nil {
            uploadImageView[2] = nil
            print("按鈕3")
        }
    }
    
    @IBAction func uploadPhoroButtonAct4(_ sender: Any) {
        uploadPhotos()
        if uploadImageView[3] != nil {
            uploadImageView[3] = nil
            print("按鈕4")
        }
    }
    
    @IBAction func uploadPhoroButtonAct5(_ sender: Any) {
        uploadPhotos()
        if uploadImageView[4] != nil {
            uploadImageView[4] = nil
            print("按鈕5")
        }
    }
    
    var successInfo = Bool()
    var apiMessage = String()
    var setImageView =  UIImage()
    var uploadImageView = [UIImage?](repeating: nil, count: 5)
    var convertString = String()
    var photoStringAry = [String]()
    
    var recommendName: String?
    var recommendAddress: String?
    var recommendPhone: String?
    var recommendOpenTime: String?
    var recommendWebsite: String?
    var recommendCost: String?
    var recommendTraffic: String?
    var recommendNote: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        recommendName = tfName.text
        recommendAddress = tfAddress.text
        recommendPhone = tfPhone.text
        recommendOpenTime = tfOpenTime.text
        recommendCost = tfCost.text
        recommendWebsite = tfWebsite.text
        recommendNote = tfNote.text
        recommendTraffic = tfTraffic.text
        
        initData()
        initEvent()
        initView()
        setValue()
        reloadScrollView()
        syncApi()
        //        tfName.attributedPlaceholder = NSAttributedString(string: "*", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red])
    }
    
    func uploadString() {
        let uploadString1 = uploadImageView[0]?.pngData()?.base64EncodedString() ?? ""
        let uploadString2 = uploadImageView[1]?.pngData()?.base64EncodedString() ?? ""
        let uploadString3 = uploadImageView[2]?.pngData()?.base64EncodedString() ?? ""
        let uploadString4 = uploadImageView[3]?.pngData()?.base64EncodedString() ?? ""
        let uploadString5 = uploadImageView[4]?.pngData()?.base64EncodedString() ?? ""
        
        photoStringAry.append(uploadString1)
        photoStringAry.append(uploadString2)
        photoStringAry.append(uploadString3)
        photoStringAry.append(uploadString4)
        photoStringAry.append(uploadString5)
    }
    
    private func initData(){
        
    }
    
    private func initEvent(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: .keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .keyboardWillHideNotification, object: nil)
        
        let arr = [tfName, tfAddress, tfPhone, tfOpenTime, tfWebsite, tfCost, tfTraffic]
        for i in 0..<arr.count {
            arr[i]?.delegate = self
            arr[i]?.tag = i
        }
    }
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    private func initView(){
        
    }
    
    
    private func setValue(){
        
    }
    
    private func reloadScrollView(){
//        var height = CGFloat(0)
//        for v in svMainView.subviews {
//            let h = v.bounds.height
//            let constraint = v.constraints.filter { $0.firstAttribute == .top || $0.secondAttribute == .top }.map { $0.constant }.sum()
//            height += h + constraint
//            log(TAG, "h \(h), \(constraint)")
//        }
//        log(TAG, "height \(height)")
//        svMainHeight.constant = height
//        svMainView.layoutIfNeeded()
        if let bottomView = svMainView.subviews.max(by: { $1.y > $0.y }) {
//            log(TAG, "bottomView \(bottomView.y + bottomView.bounds.height)")
            let constraintHeight = svMainView.constraints.first { $0.firstAttribute == .height }
            constraintHeight?.constant = bottomView.y + bottomView.bounds.height + 20
            svMainView.layoutIfNeeded()
        }
    }

    private var keyboardHeight:CGFloat?
    @objc func keyboardWillChangeFrame(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            self.keyboardHeight = CGFloat(keyboardFrame.cgRectValue.height)
            self.svMain.contentSize.height = self.vMain.height - (self.keyboardHeight ?? 0)
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let _: NSValue =
            notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            DispatchQueue.main.async {
                self.svMain.contentSize.height = self.vMain.height + (self.keyboardHeight ?? 0)
                self.svMain.setContentOffset(CGPoint(x:self.svMain.x, y:  (self.keyboardHeight ?? 0)), animated: true)
            }
        }
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        self.svMain.contentSize.height = self.svMain.contentSize.height - (self.keyboardHeight ?? 0)
        self.view.layoutIfNeeded()
        self.keyboardHeight = 0
        DispatchQueue.main.async {
            self.svMain.setContentOffset(CGPoint(x:0, y:  0), animated: true)
        }
    }
    
    func setApiValue(_ data: RecommendPageModel) {
        successInfo = data.success
        apiMessage = data.message!
    }
    
    func uploadPhotos() {
        let photoSourceRequestController = UIAlertController(title: "", message: "選擇圖片", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "開啟相機", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                imagePicker.delegate = self
                
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        
        let photoLibraryAction = UIAlertAction(title: "開啟相簿", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: .none)
        
        photoSourceRequestController.addAction(cameraAction)
        photoSourceRequestController.addAction(photoLibraryAction)
        photoSourceRequestController.addAction(cancelAction)
        photoSourceRequestController.modalPresentationStyle = .fullScreen
        
        present(photoSourceRequestController, animated: true, completion: nil)
    }
    
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.recommendPage(attraction_name: self.recommendName!, address: self.recommendAddress!, contact_info: self.recommendPhone ?? "", banking_hours: self.recommendOpenTime ?? "", attraction_url: self.recommendWebsite ?? "", payment_method: self.recommendCost ?? "", traffic_info: self.recommendTraffic ?? "", image_array: self.photoStringAry, remarks: self.recommendNote ?? "")
            if response.isSuccess {
                DispatchQueue.main.async {
                    print("資料上傳狀態 ＝ \(self.successInfo)")
                    self.uploadString()
                    self.syncApi()
                }
            }
        }
    }
}


extension RecommendViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //新增圖片
        if uploadImageView[0] == nil {
            if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
                uploadImageView[0] = selectedImage
                uploadPhoto1.image = uploadImageView[0]
                print(uploadImageView[0]!)
                print("總數 = \(uploadImageView.count)")
                print("圖片已選擇")
            }
        }else if uploadImageView[1] == nil {
            if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
                uploadImageView[1] = selectedImage
                uploadPhoto2.image = uploadImageView[1]
                print(uploadImageView[1]!)
                print("總數 = \(uploadImageView.count)")
                print("圖片已選擇")
            }
        }else if uploadImageView[2] == nil {
            if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
                uploadImageView[2] = selectedImage
                uploadPhoto3.image = uploadImageView[2]
                print(uploadImageView[2]!)
                print("總數 = \(uploadImageView.count)")
                print("圖片已選擇")
            }
        }else if uploadImageView[3] == nil {
            if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
                uploadImageView[3] = selectedImage
                uploadPhoto4.image = uploadImageView[3]
                print(uploadImageView[3]!)
                print("總數 = \(uploadImageView.count)")
                print("圖片已選擇")
            }
        }else if uploadImageView[4] == nil {
            if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
                uploadImageView[4] = selectedImage
                uploadPhoto5.image = uploadImageView[4]
                print(uploadImageView[4]!)
                print("總數 = \(uploadImageView.count)")
                print("圖片已選擇")
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
}

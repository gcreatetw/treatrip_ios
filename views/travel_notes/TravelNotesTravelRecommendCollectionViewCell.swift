//
//  TravelNotesTravelRecommendCollectionViewCell.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/24.
//

import Foundation
import UIKit

class TravelNotesTravelRecommendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var lb: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: TravelNotesPageModel.TravelRecommend){
        iv.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        lb.text = data.post_title ?? ""
    }
}

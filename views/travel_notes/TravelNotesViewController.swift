//
//  TravelNotesViewController.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/15.
//

import UIKit

class TravelNotesViewController: BaseViewController {
    
    @IBOutlet weak var svMain: UIScrollView!
    @IBOutlet weak var svMainView: UIView!
    @IBOutlet weak var collectionBanner: UICollectionView!
    @IBOutlet weak var collectionPlaymate: UICollectionView!
    @IBOutlet weak var collectionBlogger:  UICollectionView!
    @IBOutlet weak var collectionTravelRecommend:  UICollectionView!
    
    @IBAction func bannerButtonClick(_ sender: Any) {
        let controller = UIStoryboard(name: "BloggerPage", bundle: nil)
        let vc = controller.instantiateViewController(identifier: "BloggerPageViewController")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: .none)
    }
    
    
    private let bannerList = ["home_bn01", "home_bn02", "home_bn03"]
    private var playmateList = [TravelNotesPageModel.Playmate]()
    private var bloggerList = [TravelNotesPageModel.Blogger]()
    private var travelRecommendList = [TravelNotesPageModel.TravelRecommend]()
    private var page = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initData()
        initView()
        setValue()
        syncApi()
        syncApiRecommend()
    }
    
    private func initData(){
        
    }
    
    private func initView(){
        svMain.delegate = self
        collectionBanner?.apply {
            $0.dataSource = self
            $0.delegate = self
            $0.isPagingEnabled = true
        }
        collectionPlaymate?.apply {
            $0.dataSource = self
            $0.delegate = self
        }
        collectionBlogger?.apply {
            $0.dataSource = self
            $0.delegate = self
        }
        collectionTravelRecommend?.apply {
            $0.dataSource = self
            $0.delegate = self
            $0.isScrollEnabled = false
            if let layout = $0.collectionViewLayout as? UICollectionViewFlowLayout {
//                layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
//                layout.itemSize = CGSize(width: screenWidth/2, height: screenWidth/3)
                layout.minimumInteritemSpacing = 0
                layout.minimumLineSpacing = 0
                $0.collectionViewLayout = layout
            }
        }
    }
    
    private func setValue(){
        
    }

    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.postTravelNotesPage()
            if response.isSuccess {
                self.playmateList = response.data?.playmate ?? []
                self.bloggerList = response.data?.blogger ?? []
//                self.travelRecommendList = response.data?.travel_recommend ?? []
                DispatchQueue.main.async {
                    self.collectionPlaymate.reloadData()
                    self.collectionBlogger.reloadData()
//                    self.collectionTravelRecommend.reloadData() {
//                        self.reloadCollectionTravelRecommend()
//                        self.reloadScrollView()
//                    }
                }
            }
        }
    }
    
    private var isApiRecommendRunning = false
    private func syncApiRecommend(){
        if isApiRecommendRunning == true {
            return
        }
        isApiRecommendRunning = true
        page += 1
        DispatchQueue.init(label: "api").async {
            let response = ApiService.postTravelNotesPage(page: self.page)
            if response.isSuccess {
                for item in response.data?.travel_recommend ?? [] {
                    if self.travelRecommendList.contains(where: { $0.ID == item.ID }) == false {
                        self.travelRecommendList.append(item)
                    }
                }
                DispatchQueue.main.async {
                    self.collectionTravelRecommend.reloadData() {
                        self.reloadCollectionTravelRecommend()
                        self.reloadScrollView()
                        self.isApiRecommendRunning = false
                    }
                }
            }
        }
    }
    
    private func reloadCollectionTravelRecommend(){
        let constraintHeight = collectionTravelRecommend.constraints.first { $0.firstAttribute == .height && $0.firstItem === collectionTravelRecommend  }
        constraintHeight?.constant = CGFloat(165 * ((travelRecommendList.count / 2) + 2))
        collectionTravelRecommend.layoutIfNeeded()
    }
    
    private func reloadScrollView(){
        if let bottomView = svMainView.subviews.max(by: { $1.y > $0.y }) {
            let h = bottomView.constraints.first { $0.firstAttribute == .height && $0.firstItem === bottomView }?.constant ?? 0
//            log(TAG, "bottomView \(bottomView.y + h)")
            let constraintHeight = svMainView.constraints.first { $0.firstAttribute == .height }
            constraintHeight?.constant = bottomView.y + h
            svMainView.layoutIfNeeded()
        }
    }
}

extension TravelNotesViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Use this 'canLoadFromBottom' variable only if you want to load from bottom iff content > table size
        let contentSize = scrollView.contentSize.height
        let tableSize = scrollView.frame.size.height - scrollView.contentInset.top - scrollView.contentInset.bottom
        let canLoadFromBottom = contentSize > tableSize

        // Offset
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let difference = maximumOffset - currentOffset

        // Difference threshold as you like. -120.0 means pulling the cell up 120 points
        if canLoadFromBottom && difference <= -0.0 {
//        if canLoadFromBottom {

            // Save the current bottom inset
            let previousScrollViewBottomInset = scrollView.contentInset.bottom
            // Add 50 points to bottom inset, avoiding it from laying over the refresh control.
            scrollView.contentInset.bottom = previousScrollViewBottomInset + 50

            // load more data function
            syncApiRecommend()
        }
    }
}

extension TravelNotesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionBanner {
            return bannerList.count
        } else if collectionView == collectionPlaymate {
            return playmateList.count
        } else if collectionView == collectionBlogger {
            return bloggerList.count
        } else if collectionView == collectionTravelRecommend {
            return travelRecommendList.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionBanner {
            // banner 橫幅
            let cell = collectionView.dequeueReusableCell(withClass: TravelNotesBannerCollectionViewCell.self, for: indexPath)
            if indexPath.row < bannerList.count {
                cell.setValue(bannerList[indexPath.row])
            }
            return cell
        } else if collectionView == collectionPlaymate {
            // 精選
            let cell = collectionView.dequeueReusableCell(withClass: TravelNotesPlaymateCollectionViewCell.self, for: indexPath)
            if indexPath.row < playmateList.count {
                cell.setValue(playmateList[indexPath.row])
            }
            return cell
        } else if collectionView == collectionBlogger {
            // blogger
            let cell = collectionView.dequeueReusableCell(withClass: TravelNotesBloggerCollectionViewCell.self, for: indexPath)
            if indexPath.row < bloggerList.count {
                cell.setValue(bloggerList[indexPath.row])
            }
            return cell
        } else if collectionView == collectionTravelRecommend {
            // 遊記
            let cell = collectionView.dequeueReusableCell(withClass: TravelNotesTravelRecommendCollectionViewCell.self, for: indexPath)
            if indexPath.row < travelRecommendList.count {
                cell.setValue(travelRecommendList[indexPath.row])
            }
            return cell
        } else {
            // location
            let cell = collectionView.dequeueReusableCell(withClass: TravelNotesPlaymateCollectionViewCell.self, for: indexPath)
            if indexPath.row < playmateList.count {
                cell.setValue(playmateList[indexPath.row])
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionTravelRecommend {
            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.url = "https://www.treatrip.com/limited/hsinchu-lightcoming/"
                self.present(next, animated: true)
            }
        }
        if collectionView == collectionBlogger {
            let storyboard = UIStoryboard.init(name: "BloggerPage", bundle: nil)
            let bloggerData = bloggerList[indexPath.row]
            if let next = storyboard.instantiateViewController(withClass: BloggerPageViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.bloggerData = bloggerData
                next.bloggerId = Int(bloggerData.ID ?? "")
                self.present(next, animated: true)
            }
        }
    }
}

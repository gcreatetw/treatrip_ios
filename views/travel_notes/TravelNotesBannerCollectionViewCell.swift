//
//  TravelNotesBannerCollectionViewCell.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/24.
//

import UIKit

class TravelNotesBannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iv: UIImageView!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ image: String){
        iv.image = UIImage(named: image)
    }
}


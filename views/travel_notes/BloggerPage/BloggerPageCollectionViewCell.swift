//
//  BloggerCollectionViewCell.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/24.
//

import UIKit

class BloggerPageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bloggerCellImageView: UIImageView!
    @IBOutlet weak var bloggerCellLabel: UILabel!
    
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: BloggerPageModel.BloggerPosts) {
        bloggerCellImageView.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        bloggerCellLabel.text = data.post_title ?? ""
        
    }
    
}

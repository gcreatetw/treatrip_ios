//
//  BloggerViewController.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/24.
//

import UIKit

class BloggerPageViewController: UIViewController {

    private var bloggerListId = [TravelNotesPageModel.Blogger]()
    private var bloggerList = [BloggerPageModel.BloggerPosts]()
    var bloggerId: Int?
  
    @IBOutlet weak var bloggerImageView: UIImageView!
    
    @IBOutlet weak var bloggerLabel: UILabel!
    
    @IBOutlet weak var bloggerUrlLabel: UIButton!
    
    @IBOutlet weak var bloggerCollectionView: UICollectionView!
    
    var bloggerData: TravelNotesPageModel.Blogger?
    
    
    @IBAction func toGoButtonAct(_ sender: Any) {
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
        syncApi()
        bloggerImageView.image = UIImage(named: bloggerData?.post_img ?? "")
        bloggerLabel.text = bloggerData?.post_title
    }
    
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.bloggerPage(post_id: self.bloggerId ?? 0)
            print("BloggerID = \(self.bloggerId!)")
            if response.isSuccess {
                self.bloggerList = response.data?.bloger_posts ?? []
                DispatchQueue.main.async {
                    self.bloggerCollectionView.reloadData()
                    self.bloggerImageView.kf.setImage(with: URL(string: self.bloggerData?.post_img? .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
                }
            }
        }
    }

    private func initView() {
        bloggerCollectionView.apply {
            $0.dataSource = self
            $0.delegate = self
        }
    }
}

extension BloggerPageViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bloggerList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: BloggerPageCollectionViewCell.self, for: indexPath)
        
        if indexPath.row < bloggerList.count {
            cell.setValue(bloggerList[indexPath.row])
        }
        
        let bloggerData = bloggerList[indexPath.row]
        cell.bloggerCellLabel.text = bloggerData.post_title
        cell.bloggerCellImageView.kf.setImage(with: URL(string: bloggerData.post_img ?? ""))
        
//        let idData = bloggerListId[indexPath.row]
//        bloggerId = Int(idData.ID ?? "") ?? 0
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bloggerCollectionView {
            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
            let bloggerData = bloggerList[indexPath.row]
            
            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.url = bloggerData.post_url ?? ""
                self.present(next, animated: true)
            }
        }
    }
}

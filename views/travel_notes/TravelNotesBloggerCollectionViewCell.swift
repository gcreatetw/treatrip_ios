//
//  TravelNotesBloggerCollectionViewCell.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/24.
//

import UIKit

class TravelNotesBloggerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbBloggerJobTitle: UILabel!
  
    override func awakeFromNib() {
        initView()
    }
   
    func initView(){
        
    }
    
    func setValue(_ data: TravelNotesPageModel.Blogger){
        iv.kf.setImage(with: URL(string: data.post_img?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        lbTitle.text = data.post_title ?? ""
        lbBloggerJobTitle.text = data.blogger_job_title ?? ""
        if data.blogger_job_title == "" {
            lbBloggerJobTitle.text = "部落客"
        }
    }
}

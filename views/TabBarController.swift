//
//  TabBarController.swift
//  SampleApp
//
//  Created by Aki Wang on 2020/10/22.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
           // Always adopt a light interface style.
           overrideUserInterfaceStyle = .light
       }
        initTab()
    }

    private func initTab(){
        // 設定 item
        var vcAry = [UIViewController]()
        
//        if let vc = UIStoryboard(name: "Tab1", bundle:nil).instantiateViewController(withClass: StationViewController.self) {
//            vc.tabBarItem =
//                UITabBarItem(title: "景點",image: UIImage(svg_name: "icon_attractions"),selectedImage: UIImage(svg_name: "icon_attractions"))
//            arr.append(vc)
//        }
        
        if let vc = UIStoryboard(name: "Home", bundle:nil).instantiateViewController(withClass: HomeViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "景點",image: UIImage(svg_name: "icon_attractions"),selectedImage: UIImage(svg_name: "icon_attractions"))
            vcAry.append(vc)
        }
        
        if let vc = UIStoryboard(name: "TravelNotes", bundle:nil).instantiateViewController(withClass: TravelNotesViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "遊記",image: UIImage(svg_name: "icon_travel-notes"),selectedImage: UIImage(svg_name: "icon_travel-notes"))
            vcAry.append(vc)
        }
        
//        if let vc = UIStoryboard(name: "Good", bundle: nil).instantiateViewController(withClass: GoodViewController.self) {
//            vc.tabBarItem =
//                UITabBarItem(title: "好康", image: UIImage(svg_name: "icon_good"), selectedImage: UIImage(svg_name: "icon_good"))
//            vcAry.append(vc)
//        }
        
//        if let vc = UIStoryboard(name: "Member", bundle: nil).instantiateViewController(withClass: MemberViewController.self) {
//            vc.tabBarItem =
//                UITabBarItem(title: "會員", image: UIImage(svg_name: "icon_member"), selectedImage: UIImage(svg_name: "icon_member"))
//            vcAry.append(vc)
//        }
        
        if let vc = UIStoryboard(name: "Recommend", bundle:nil).instantiateViewController(withClass: RecommendViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "推薦",image: UIImage(svg_name: "icon_recommend"),selectedImage: UIImage(svg_name: "icon_recommend"))
            vcAry.append(vc)
        }
        
        // set view contriller
        self.viewControllers = vcAry
        // setting tab ber style
        //去除 tab bar 上方黑線
        tabBar.layer.borderWidth = 0
        tabBar.clipsToBounds = true
        //設定selected and unselected icon 顏色
        tabBar.tintColor = .def_tab_selected
        tabBar.unselectedItemTintColor = .def_tab_unselected
        //
        changeTabItemBackground(tabBar, selectedIndex, .def_tab_bg_selected)
    }
    
    //Tabbar選擇(暫)
    private func changeTabItemBackground(_ tabBar: UITabBar, _ tabIndex: Int, _ backgroundColor: UIColor) {
//        tabBar.subviews.filter({ $0.layer.name == "bgView" }).first?.removeFromSuperview()
//        tabBar.subviews.filter({ $0.layer.name == "topLineView" }).first?.removeFromSuperview()
//        if let items = tabBar.items {
//            let tabIndex = CGFloat(tabIndex)
//            let tabWidth = tabBar.bounds.width / CGFloat(items.count)
//            //
//            let bgView = UIView(frame: CGRect(x: tabWidth * tabIndex, y: 0, width: tabWidth, height: 1000))
//            bgView.backgroundColor = backgroundColor
//            bgView.layer.name = "bgView"
//            tabBar.insertSubview(bgView, at: 0)
//            //
//            let border = CALayer()
//            border.backgroundColor = UIColor.def_tab_selected.cgColor
//            border.frame = CGRect(x: 0, y: 0, width: bgView.frame.width, height: 3)
//            bgView.layer.addSublayer(border)
//        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        log(TAG, "\(item.title)")
        changeTabItemBackground(tabBar, tabBar.items?.firstIndex(of: item) ?? 0, .def_tab_bg_selected)
    }
}

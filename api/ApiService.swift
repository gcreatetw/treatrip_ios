//
//  ApiService.swift
//  ChargerApp
//
//  Created by Aki Wang on 2020/10/13.
//

import Foundation

import Foundation
import Alamofire

struct ApiService{
    static var defApiAddress = "https://www.treatrip.com/wp-json/treatrip_api/"
    static var baseURL: String {
        get{
            return UserDefaultsManager.getInstance().getApiAddress()
        }
    }
    
    /** sample 同步 */ //這個比較常會用到
    static func Sample(email: String, password: String) -> ApiResponse<LoginModel>{
        let url = "\(baseURL)auth/login"//URL
        let headers = [
            "Authorization": "Bearer \(UserDefaultsManager.getInstance().getApiToken())",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters = [
            "email" : "\(email)",
            "password" : "\(password)"
        ]
        let result: ApiResponse<LoginModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    /** sample 異步 */  //這個不太會用到
    static func Sample(email: String, password: String, callBack: @escaping  (ApiResponse<LoginModel>) -> Void){
        let url = "\(baseURL)auth/login"//URL
        let headers = [
            "Authorization": "Bearer \(UserDefaultsManager.getInstance().getApiToken())",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters = [
            "email" : "\(email)",
            "password" : "\(password)"
        ]
        //異步
        ApiTool.request(url: url, method: .post, parameters: parameters , headers: headers, callBack: callBack)
    }
    
    /** 登入 */
    static func login(account: String, password: String) -> ApiResponse<LoginModel>{
        let url = "\(baseURL)auth/login"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["device_token"] = UserDefaultsManager.getInstance().getDeviceToken()
        parameters["account"] = account
        parameters["password"] = password
        parameters["system"] = "ios"
        let result: ApiResponse<LoginModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    /** 台北市 youbike  **/
    static func getTapieiYouBikeStation() -> ApiResponse<TapipeiYouBikeStationModel>{
        let url = "https://tcgbusfs.blob.core.windows.net/blobyoubike/YouBikeTP.json"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let result: ApiResponse<TapipeiYouBikeStationModel> = ApiTool.execute(url: url, method: .get, headers: headers)
        return result
    }
    
    static func postHomePage() -> ApiResponse<HomePageModel>{
        let url = "\(baseURL)home_page"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters = [String:Any]()
        //跑到result API讀完了，alamo request 會額外開執行緒
        let result: ApiResponse<HomePageModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func postTravelNotesPage() -> ApiResponse<TravelNotesPageModel>{
        let url = "\(baseURL)travel_notes_page"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters = [String:Any]()
        let result: ApiResponse<TravelNotesPageModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func postTravelNotesPage(page: Int) -> ApiResponse<TravelRecommendModel>{
        let url = "\(baseURL)travel_recommend"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["page"] = page
        let result: ApiResponse<TravelRecommendModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func postHomeAdventureTheme(page: Int) -> ApiResponse<HomeAdventureThemeModel>{
        let url = "\(baseURL)adventure"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["page"] = page
        let result: ApiResponse<HomeAdventureThemeModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func explorePage() -> ApiResponse<ExploreModel>{
        let url = "\(baseURL)discover_taiwain"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters = [String:Any]()
        let result: ApiResponse<ExploreModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func bloggerPage(post_id: Int) -> ApiResponse<BloggerPageModel>{
        let url = "\(baseURL)blog_posts"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["post_id"] = post_id
        let result: ApiResponse<BloggerPageModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func taiwanDetailPage(post_id: String) -> ApiResponse<TaiwanDetailModel>{
        let url = "\(baseURL)post_info"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["post_id"] = post_id
        let result: ApiResponse<TaiwanDetailModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func countyPage(region: String, id: Int) -> ApiResponse<CountyPageModel>{
        let url = "\(baseURL)region"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["region"] = region
        parameters["id"] = id
        let result: ApiResponse<CountyPageModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func recommendPage(attraction_name: String, address: String, contact_info: String?, banking_hours: String?, attraction_url: String?, payment_method: String?, traffic_info: String?, image_array: [String], remarks: String? ) -> ApiResponse<RecommendPageModel>{
        let url = "\(baseURL)recommend_scenic_spot"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["attraction_name"] = attraction_name
        parameters["contact_info"] = contact_info
        parameters["banking_hours"] = banking_hours
        parameters["attraction_url"] = attraction_url
        parameters["payment_method"] = payment_method
        parameters["traffic_info"] = traffic_info
        parameters["image_array"] = image_array
        parameters["remarks"] = remarks
        
        let result: ApiResponse<RecommendPageModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
}

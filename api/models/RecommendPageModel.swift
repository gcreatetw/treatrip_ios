//
//  RecommendPageModel.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/6/2.
//

import Foundation

struct RecommendPageModel: Codable {
    
    let success: Bool
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)!
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
    
}

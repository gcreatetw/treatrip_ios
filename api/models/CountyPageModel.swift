//
//  CountyPageModel.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/31.
//

import Foundation

struct CountyPageModel: Codable {
    let region_banner_img: String?
    let success: Bool?
    let towns: [Towns]?
    
    enum CodingKeys: String, CodingKey {
        case region_banner_img = "region_banner_img"
        case success = "success"
        case towns = "towns"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        region_banner_img = try values.decodeIfPresent(String.self, forKey: .region_banner_img)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        towns = try values.decodeIfPresent([Towns].self, forKey: .towns)
    }
    
    struct Towns: Codable {
        let township: String?
        let recommendAttractions: [RecommendAttractions]?
        
        enum CodingKeys: String, CodingKey {
            case township = "township"
            case recommendAttractions = "Recommend_Attractions"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            township = try values.decodeIfPresent(String.self, forKey: .township)
            recommendAttractions = try values.decodeIfPresent([RecommendAttractions].self, forKey: .recommendAttractions)
        }
    }
    
    struct RecommendAttractions: Codable {
        let ID: Int?
        let post_title: String?
        let post_img: String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
        }
    }
}

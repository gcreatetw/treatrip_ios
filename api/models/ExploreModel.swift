//
//  ExploreModel.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/20.
//

import Foundation

struct ExploreModel: Codable {
    
    let success: Bool?
    let BN: String?
    let discover_taiwain: [DiscoverTaiwan]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case BN = "BN"
        case discover_taiwain = "discover_taiwain"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        BN = try values.decode(String.self, forKey: .BN)
        discover_taiwain = try values.decodeIfPresent([DiscoverTaiwan].self, forKey: .discover_taiwain)
    }
    
    struct DiscoverTaiwan: Codable {
        let main_type: String?
        let icon: String?
        let catgories: [Catgories]?
        
        enum CodingKeys: String, CodingKey {
            case main_type = "main_type"
            case icon = "icon"
            case catgories = "catgories"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            main_type = try values.decodeIfPresent(String.self, forKey: .main_type)
            icon = try values.decode(String.self, forKey: .icon)
            catgories = try values.decodeIfPresent([Catgories].self, forKey: .catgories)
        }
        
    }
    
    struct Catgories: Codable {
        let cat_title: String?
        let cat_posts: [CatPosts]?
        
        enum CodingKeys: String, CodingKey {
            case cat_title = "cat_title"
            case cat_posts = "cat_posts"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            cat_title = try values.decodeIfPresent(String.self, forKey: .cat_title)
            cat_posts = try values.decode([CatPosts].self, forKey: .cat_posts)
        }
    }
    
    struct CatPosts: Codable {
        let ID: Int?
        let post_title: String?
        let post_img: String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
        }
    }
    
}

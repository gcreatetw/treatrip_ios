//
//  TravelRecommendModel.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/23.
//

import Foundation

struct TravelRecommendModel : Codable {
    
    let success : Bool?
    let travel_recommend : [TravelNotesPageModel.TravelRecommend]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case travel_recommend = "travel_recommend"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        travel_recommend = try values.decodeIfPresent([TravelNotesPageModel.TravelRecommend].self, forKey: .travel_recommend)
    }
}

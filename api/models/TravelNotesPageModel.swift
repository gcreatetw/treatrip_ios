//
//  TravelNotesPageModel.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/23.
//

import Foundation

struct TravelNotesPageModel : Codable {
    
    let success : Bool?
    let playmate : [Playmate]?
    let blogger : [Blogger]?
    let travel_recommend : [TravelRecommend]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case playmate = "playmate"
        case blogger = "blogger"
        case travel_recommend = "travel_recommend"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        playmate = try values.decodeIfPresent([Playmate].self, forKey: .playmate)
        blogger = try values.decodeIfPresent([Blogger].self, forKey: .blogger)
        travel_recommend = try values.decodeIfPresent([TravelRecommend].self, forKey: .travel_recommend)
    }
    
    struct Playmate: Codable  {
        let ID : Int?
        let post_title : String?
        let post_img : String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
        }
    }
    
    struct Blogger: Codable  {
        let ID : String?
        let post_title : String?
        let post_img : String?
        let blogger_job_title : String?
        let bloger_id : String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
            case blogger_job_title = "blogger_job_title"
            case bloger_id = "bloger_id"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(String.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
            blogger_job_title = try values.decodeIfPresent(String.self, forKey: .blogger_job_title)
            bloger_id = try values.decodeIfPresent(String.self, forKey: .bloger_id)
        }
    }
    
    struct TravelRecommend: Codable  {
        let ID : Int?
        let post_title : String?
        let post_img : String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
        }
    }
}

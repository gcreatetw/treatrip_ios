//
//  BloggerModel.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/24.
//

import Foundation

struct BloggerPageModel: Codable {
    
    let success: Bool?
    let bloger_url: String?
    let bloger_posts: [BloggerPosts]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case bloger_url = "bloger_url"
        case bloger_posts = "bloger_posts"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        bloger_url = try values.decodeIfPresent(String.self, forKey: .bloger_url)
        bloger_posts = try values.decodeIfPresent([BloggerPosts].self, forKey: .bloger_posts)
    }
    
    struct BloggerPosts: Codable {
        let ID: Int?
        let post_title: String?
        let post_img: String?
        let post_url: String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
            case post_url = "post_url"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
            post_url = try values.decodeIfPresent(String.self, forKey: .post_url)
        }
    }
    
    
}

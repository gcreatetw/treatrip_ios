//
//  HomePageModel.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/17.
//

import Foundation

struct HomePageModel : Codable {
    
    let success : Bool?
    let limited_time : [LimitedTime]?
    let popular_attractions : [PopularAttractions]?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case limited_time = "limited_time"
        case popular_attractions = "popular_attractions"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        limited_time = try values.decodeIfPresent([LimitedTime].self, forKey: .limited_time)
        popular_attractions = try values.decodeIfPresent([PopularAttractions].self, forKey: .popular_attractions)
    }
    
    struct LimitedTime: Codable  {
        let ID : Int?
        let post_title : String?
        let post_img : String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
        }
    }
    
    struct PopularAttractions: Codable  {
        let ID : Int?
        let post_title : String?
        let post_img : String?
        
        enum CodingKeys: String, CodingKey {
            case ID = "ID"
            case post_title = "post_title"
            case post_img = "post_img"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            ID = try values.decodeIfPresent(Int.self, forKey: .ID)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent(String.self, forKey: .post_img)
        }
    }
}

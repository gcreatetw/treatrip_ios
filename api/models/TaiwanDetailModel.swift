//
//  TaiwanDetailModel.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/27.
//

import Foundation

struct TaiwanDetailModel: Codable {
    
    let success: Bool?
    let post_info: PostInfo?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case post_info = "post_info"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        post_info = try values.decodeIfPresent(PostInfo.self, forKey: .post_info)
    }
    
    struct PostInfo: Codable {
        let post_date: Date?
        let post_content: String?
        let post_title: String?
        let post_img: [String]?
        let region: String?
        let town: String?
        let add: String?
        let tel: String?
        let travellinginfo: String?
        let opentime: String?
        let website: String?
        let img_form: String?
        let py: String?
        let px: String?
        let tag: [String]?
        let cat: [String]?
        
        enum CodingKeys: String, CodingKey {
            case post_date = "post_date"
            case post_content = "post_content"
            case post_title = "post_title"
            case post_img = "post_img"
            case region = "region"
            case town = "town"
            case add = "add"
            case tel = "tel"
            case travellinginfo = "travellinginfo"
            case opentime = "opentime"
            case website = "website"
            case img_form = "img_form"
            case py = "py"
            case px = "px"
            case tag = "tag"
            case cat = "cat"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            post_date = try values.decodeIfPresent(Date.self, forKey: .post_date)
            post_content = try values.decodeIfPresent(String.self, forKey: .post_content)
            post_title = try values.decodeIfPresent(String.self, forKey: .post_title)
            post_img = try values.decodeIfPresent([String].self, forKey: .post_img)
            region = try values.decodeIfPresent(String.self, forKey: .region)
            town = try values.decodeIfPresent(String.self, forKey: .town)
            add = try values.decodeIfPresent(String.self, forKey: .add)
            tel = try values.decodeIfPresent(String.self, forKey: .tel)
            travellinginfo = try values.decodeIfPresent(String.self, forKey: .travellinginfo)
            opentime = try values.decodeIfPresent(String.self, forKey: .opentime)
            website = try values.decodeIfPresent(String.self, forKey: .website)
            img_form = try values.decodeIfPresent(String.self, forKey: .img_form)
            py = try values.decodeIfPresent(String.self, forKey: .py)
            px = try values.decodeIfPresent(String.self, forKey: .px)
            tag = try values.decodeIfPresent([String].self, forKey: .tag)
            cat = try values.decodeIfPresent([String].self, forKey: .cat)
        }
    }
    
    
}

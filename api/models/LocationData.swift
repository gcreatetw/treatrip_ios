//
//  LocationData.swift
//  Treatrip
//
//  Created by TANG,QI-RONG on 2021/5/31.
//

import Foundation

class LocationData {
    
    static let share: LocationData = {
        let instance = LocationData()
        
        return instance
    }()
    
    private init() {}
    
     let data = [
        HomeLocationData("臺北", "Taipei", "image_cover_taipei02", id: 1792 ),
        HomeLocationData("離島", "Outisland", "image_cover_outlying-islands01", id: 2371),
        HomeLocationData("宜蘭", "Yilan", "image_cover_yilan01", id: 2367),
        HomeLocationData("臺中", "Taichung", "image_cover_taichung", id: 30902),
        HomeLocationData("新竹", "Hsinchu", "image_cover_hsinchu01", id: 2369),
        HomeLocationData("苗栗", "Miaoli", "image_cover_miaoli", id: 32247),
        HomeLocationData("花蓮", "Hualien", "image_cover_hualien", id: 2373),
        HomeLocationData("新北", "Xinbei", "image_cover_xinbei", id: 32428),
        HomeLocationData("臺東", "Taitung", "image_cover_taitung01", id: 2372),
        HomeLocationData("基隆", "Keelung", "image_cover_keelung", id: 32721),
        HomeLocationData("臺南", "Tainan", "image_cover_tainan", id: 36850),
        HomeLocationData("彰化", "Changhua", "image_cover_changhua", id: 32887),
        HomeLocationData("桃園", "Taoyuan", "image_cover_taoyuan01", id: 30501),
        HomeLocationData("南投", "Nantou", "image_cover_nantou", id: 35807),
        HomeLocationData("雲林", "Yunlin", "image_cover_yunlin", id: 36365),
        HomeLocationData("高雄", "Kaohsiung", "image_cover_kaohsiung", id: 37212),
        HomeLocationData("嘉義", "Chiayi", "image_cover_chiayi", id: 36536),
        HomeLocationData("屏東", "Pingtung", "image_cover_pingtung", id: 37367)
    
    ]
}
